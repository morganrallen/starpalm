EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED:WS2812B D1
U 1 1 5C238D11
P 5150 2675
F 0 "D1" H 5491 2721 50  0000 L CNN
F 1 "WS2812B" H 5491 2630 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5200 2375 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5250 2300 50  0001 L TNN
	1    5150 2675
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D2
U 1 1 5C238D6E
P 6450 2675
F 0 "D2" H 6791 2721 50  0000 L CNN
F 1 "WS2812B" H 6791 2630 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 6500 2375 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 6550 2300 50  0001 L TNN
	1    6450 2675
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D3
U 1 1 5C238EA6
P 7650 2675
F 0 "D3" H 7991 2721 50  0000 L CNN
F 1 "WS2812B" H 7700 2400 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 7700 2375 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 7750 2300 50  0001 L TNN
	1    7650 2675
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2675 6150 2675
Wire Wire Line
	6750 2675 7350 2675
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5C238F91
P 3275 2675
F 0 "J1" H 3381 2953 50  0000 C CNN
F 1 "Conn_01x04_Male" H 3381 2862 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 3275 2675 50  0001 C CNN
F 3 "~" H 3275 2675 50  0001 C CNN
	1    3275 2675
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2575 4625 2575
Wire Wire Line
	4625 2575 4625 2250
Wire Wire Line
	4625 2250 5150 2250
Wire Wire Line
	5150 2250 5150 2375
Wire Wire Line
	4300 2675 4850 2675
Wire Wire Line
	4725 3100 5150 3100
Wire Wire Line
	6450 3100 6450 2975
Wire Wire Line
	6450 2250 6450 2375
Wire Wire Line
	7650 2250 7650 2375
Wire Wire Line
	6450 3100 7650 3100
Wire Wire Line
	7650 3100 7650 2975
Connection ~ 6450 3100
Wire Wire Line
	7950 2675 8175 2675
Wire Wire Line
	8175 2675 8175 3150
Wire Wire Line
	8175 3150 4625 3150
Wire Wire Line
	5150 3100 6450 3100
Connection ~ 5150 3100
Wire Wire Line
	5150 2975 5150 3100
Wire Wire Line
	5150 2250 6450 2250
Connection ~ 5150 2250
Wire Wire Line
	6450 2250 7650 2250
Connection ~ 6450 2250
Wire Wire Line
	4625 2775 4300 2775
Wire Wire Line
	4625 2775 4625 3150
Wire Wire Line
	4725 2875 4725 3100
Wire Wire Line
	4300 2875 4725 2875
Text GLabel 4300 2575 0    50   Input ~ 0
VDD
Text GLabel 4300 2675 0    50   Input ~ 0
DIN
Text GLabel 4300 2875 0    50   Input ~ 0
VSS
Text GLabel 4300 2775 0    50   Input ~ 0
DOUT
Text GLabel 3475 2575 2    50   Input ~ 0
VSS
Text GLabel 3475 2775 2    50   Input ~ 0
VDD
Text GLabel 3475 2675 2    50   Input ~ 0
DIN
Text GLabel 3475 2875 2    50   Input ~ 0
DOUT
$EndSCHEMATC
