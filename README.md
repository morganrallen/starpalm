# ["Venusian Star Palm"](https://hackaday.io/project/163043-venusian-star-palm)

Hackaday Circuit Sculpture Contest [entry](https://hackaday.io/project/163043-venusian-star-palm).

* Home etched Pyralux palm fronds
* Mixed bare copper and enamel coated wire
* Brass tubing with Kapton insulation
* Copper clad board
